//
//  GameViewController.h
//  Pong
//
//  Created by MattiasO on 2015-02-15.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *StartGameButton;
@property (weak, nonatomic) IBOutlet UIImageView *Puck;
@property (weak, nonatomic) IBOutlet UIImageView *Computer;
@property (weak, nonatomic) IBOutlet UIImageView *Player;
@property (weak, nonatomic) IBOutlet UILabel *PlayerScore;
@property (weak, nonatomic) IBOutlet UILabel *ComputerScore;
@property (weak, nonatomic) IBOutlet UILabel *ScoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *ExitButton;
@property (nonatomic) NSTimer *Timer;
@property (nonatomic) int X;
@property (nonatomic) int Y;
@property (nonatomic) int ComputerNumber;
@property (nonatomic) int PlayerNumber;
@property (nonatomic) int TotalNumber;
@property (nonatomic) double Speed;
@property (nonatomic) double ComputerSpeed;

-(void)PuckMovement;
-(void)ComputerMovement;
-(void)Collision;
@end
