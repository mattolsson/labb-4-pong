//
//  GameViewController.m
//  Pong
//
//  Created by MattiasO on 2015-02-15.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()


@end

@implementation GameViewController

- (IBAction)StartGameButton:(id)sender {
    
    self.StartGameButton.hidden = YES;
    self.ExitButton.hidden = YES;
    self.Y = arc4random() %11;
    self.Y = self.Y - 5;
    
    self.X = arc4random() %11;
    self.X = self.X - 2;
    
    if(self.Y == 0) {
        self.Y = 1;
    }
    if(self.X == 0) {
        self.X = 1;
    }
    
    if(self.TotalNumber < 2) {
        self.Speed = 0.01;
    }else if(self.TotalNumber >= 12) {
        self.Speed = 0.006;
    }else if(self.TotalNumber >= 8) {
        self.Speed = 0.007;
    }else if(self.TotalNumber >= 5) {
        self.Speed = 0.008;
    }else if(self.TotalNumber >= 2) {
        self.Speed = 0.009;
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.Speed target:self selector:@selector(PuckMovement) userInfo:nil repeats:YES];
    
}

-(void)PuckMovement {
    
    [self ComputerMovement];
    [self Collision];
    self.Puck.center = CGPointMake(self.Puck.center.x + self.X, self.Puck.center.y + self.Y);
    
    
    if(self.Puck.center.x > self.view.frame.size.width || self.Puck.center.x < 0) {
        self.X = 0 - self.X;
    }
    
    if(self.Puck.center.y < 0) {
        self.PlayerNumber++;
        self.TotalNumber++;
        self.PlayerScore.text = [NSString stringWithFormat:@"%i", self.PlayerNumber];
        
        [self.Timer invalidate];
        self.StartGameButton.hidden = NO;
        self.ExitButton.hidden = NO;
        
        self.Puck.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
        self.Computer.center = CGPointMake(self.view.frame.size.width/2, 30);
        
        if(self.PlayerNumber == 10) {
            self.StartGameButton.hidden = YES;
            self.ExitButton.hidden = NO;
            self.ScoreLabel.hidden = NO;
            self.ScoreLabel.text = [NSString stringWithFormat:@"You win!"];
        }
    }
    
    if(self.Puck.center.y > self.view.frame.size.height) {
        self.ComputerNumber++;
        self.TotalNumber++;
        self.ComputerScore.text = [NSString stringWithFormat:@"%i", self.ComputerNumber];
        
        [self.Timer invalidate];
        self.StartGameButton.hidden = NO;
        self.ExitButton.hidden = NO;
        
        self.Puck.center = CGPointMake(self.view.frame.size.width/2, self.view.bounds.size.height/2);
        self.Computer.center = CGPointMake(self.view.frame.size.width/2, 30);
        
        if(self.ComputerNumber == 10) {
            self.StartGameButton.hidden = YES;
            self.ExitButton.hidden = NO;
            self.ScoreLabel.hidden = NO;
            self.ScoreLabel.text = [NSString stringWithFormat:@"You lose!"];
        }
    }
}

-(void)ComputerMovement {
    if(self.Puck.center.y <= self.view.center.y) {
        if(self.Puck.center.x > self.Computer.center.x) {
            self.Computer.center = CGPointMake(self.Computer.center.x + self.ComputerSpeed, self.Computer.center.y);
        }
        if(self.Puck.center.x < self.Computer.center.x) {
            self.Computer.center = CGPointMake(self.Computer.center.x - self.ComputerSpeed, self.Computer.center.y);
        }
    }
    
    
    /*if (self.Computer.center.x < 50) {
        self.Computer.center = CGPointMake(50, self.Computer.center.y);
    }
    if (self.Computer.center.x > 325) {
        self.Computer.center = CGPointMake(325, self.Computer.center.y);
    }*/
}
    
-(void)Collision {
    if(CGRectIntersectsRect(self.Puck.frame, self.Player.frame)) {
        if(self.Puck.center.y < self.Player.center.y) {
            if(self.Puck.center.x > self.Player.center.x+10) {
                self.X = 5;
                self.Y = 0 - self.Y;
            }else if(self.Puck.center.x < self.Player.center.x-10) {
                self.X = - 5;
                self.Y = 0 - self.Y;
            }else if(self.Puck.center.x < self.Player.center.x+5) {
                self.X = 3;
                self.Y = 0 - self.Y;
            }else if(self.Puck.center.x < self.Player.center.x-5) {
                    self.X = - 3;
                    self.Y = 0 - self.Y;
            }else {
                self.X = self.X - self.X;
                self.Y = 0 - self.Y;
            }
        }
    }
    if(CGRectIntersectsRect(self.Puck.frame, self.Computer.frame)) {
        if(self.Puck.center.y > self.Computer.center.y) {
            self.Y = 0 - self.Y;
        }
    }
}
    
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *Move = [[event allTouches] anyObject];
    CGPoint Location = [Move locationInView:Move.view];
    self.Player.center = CGPointMake(Location.x + self.ComputerSpeed, self.Player.center.y);
    
    
    
    /*if(self.Player.center.y != 622) {
        self.Player.center = CGPointMake(self.Player.center.x, 622);
    }
    
    if (self.Player.center.x < 50) {
        self.Player.center = CGPointMake(50, self.Player.center.y);
    }
    if (self.Player.center.x > 325) {
        self.Player.center = CGPointMake(325, self.Player.center.y);
    }*/
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.PlayerNumber = 0;
    self.ComputerNumber = 0;
    self.ComputerSpeed = 2;
    self.TotalNumber = 0;
}

- (void)viewDidLayoutSubviews {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
